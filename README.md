NuclearThroneStreamer is a tiny multi-platform tool that is used to show current game stats while you play the Steam version of Nuclear Throne. This is useful for streamers who need to fill in the border space in the game. It does this by using the stream key function of the game. Every 15 seconds, the game will send information to a website, which this tool then downloads, parses and outputs to a file. OBS can then be configured to read this file and show the text on screen.


To set it up, do the following:

1. Install the latest Python 3 from https://www.python.org

2. Run NuclearThroneStreamer.py from this site.

3. Enter your Steam64id. There are websites that can find this for you, or you can use the instructions here: https://nodecraft.com/support/games/sevendaystodie/how-to-quickly-find-steam-id-numbers

4. Enter your Stream Key. This can be obtained by opening up the game, going to Settings, Game, Stream Key.

5. Click the Text Output button to pick a file to output the text to.

6. Add a new text source in OBS to read outputted file.

7. Click the red Stopped button at the top to start it and you're good to go!


If you want to change the text output, currently the only way to do that is to modify the code in the getTextOut() function. I can help you do this if you want, just contact me. For more information on the stream key functionality and what information you can get from it, see https://nuclear-throne.fandom.com/wiki/Stream_Keys